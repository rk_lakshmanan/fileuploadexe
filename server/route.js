// Load all required modules for uploading files
var fs = require("fs"),
    path = require("path"),
    multer = require("multer"),
    storage = multer.diskStorage({
        // Storage configuration for multer - where to save and under what name
        destination: './uploads_tmp/',
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    }),
    upload = multer({
        // Actual upload function using previously defined configuration
        storage: storage
    });

// Export the module to be used(AKA required) in app.js
module.exports = function (app) {
    // POST request handler for /upload route 
    app.post("/upload", upload.array("files"), function (req, res) {
        // Use fs to read the uploaded file to verfiy success
        console.log("server post")
        console.log(req.files);
        for (var i = 0; i < req.files.length; i++) {
            fs.readFile(req.files[i].path, function (err, data) {
                // Error handler for reading file
                console.log("read file")
                if (err) {
                    console.info("ERR >> " + err);
                }
                // Sends a HTTP 202 status code for Accepted to browser and a json containing the image file size to be displayed in the flash message

            });

        }
        res.status(202).json({
            size: req.files[0].size
        });

    });
}