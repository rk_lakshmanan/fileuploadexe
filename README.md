# Data Object has default fields #

### Client: ###
1. "file" : <single file>
2. "files" : <multiple files> - array

### Server: ###
1. upload.single("file")
2. upload.array("files")